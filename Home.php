<?php

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		/* if ($this->session->userdata('level') != '2') {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
		    Anda Belum Login!
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		 } */
		$this->load->model('mdata');
	}

	public function index()
	{
		$this->load->view('home');
	}

	public function index2()
	{
		$data = array(
			'judul' => 'Buat Surat Online',
			'data_surat' => $this->mdata->data_surat(),
		);
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('vhome', $data);
		$this->load->view('templates/footer');
	}

	public function buatsurat($id)
	{
		$where = array('idkategorisurat' => $id);
		$data = array(
			'data_surat' => $this->mdata->edit_data($where, 'kategoridatasurat')->result(),
		);
		$this->load->view('formsurat', $data);
	}

	public function hapusriwayat($idsurat)
	{
		$where = array('idsurat' => $idsurat);
		$this->msurat->hapus_data($where, 'riwayatsurat');
		redirect('home/indexriwayatsurat');
	}

	public function inputsurat()
	{
		$idsurat = $this->input->post('idsurat');
		$idkategorisurat = $this->input->post('idkategorisurat');
		$isi = $this->input->post('isi');
		$tanggal = $this->input->post('tanggal');
		$kepada = $this->input->post('kepada');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			// 'idpinjam' => 'idpinjam' + 1,
			'idsurat' => $idsurat,
			'idkategorisurat' => $idkategorisurat,
			'isi' => $isi,
			'tanggal' => $tanggal,
			'kepada' => $kepada,
			'keterangan' => $keterangan,
		);

		$this->msurat->input_data($data, 'riwayatsurat');
		redirect('home/indexriwayatsurat');
	}

	public function indexriwayatsurat()
	{
		$data = array(
			'judul' => 'Riwayat Peminjaman',
			'data_riwayatsurat' => $this->msurat->data_riwayatsurat(),
		);
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('riwayatpinjam', $data);
		$this->load->view('templates/footer');
	}

	public function editriwayat($id)
	{
		$where = array('idsurat' => $id);
		$data = array(
			'data_riwayatsurat' => $this->msurat->edit_data($where, 'riwayatsurat')->result(),
		);
		$this->load->view('editriwayatpinjam', $data);
	}

	public function cetakriwayat($id)
	{
		$where = array('idsurat' => $id);
		$data = array(
			'data_riwayatsurat' => $this->msurat->edit_data($where, 'riwayatsurat')->result(),
		);
		$this->load->view('cetak', $data);
	}

	public function updateriwayat()
	{
		$idsurat = $this->input->post('idsurat');
		$idkategorisurat = $this->input->post('idkategorisurat');
		$isi = $this->input->post('isi');
		$tanggal = $this->input->post('tanggal');
		$kepada = $this->input->post('kepada');
		$keterangan = $this->input->post('keterangan');

		$data = array(
			'idsurat' => $idsurat,
			'idkategorisurat' => $idkategorisurat,
			'isi' => $isi,
			'tanggal' => $tanggal,
			'kepada' => $kepada,
			'keterangan' => $keterangan,
		);

		$where = array('idsurat' => $idsurat);
		$this->msurat->update_data($where, $data, 'riwayatsurat');
		redirect('home/indexriwayatsurat');
	}
}

